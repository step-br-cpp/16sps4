#include <iostream>
#include <cstdint>

class C3 {
public:
	C3(int16_t p_i)
		: m_i(p_i) {}
	~C3() {
		std::cout << "C3 morrendo..." << std::endl;
	}
private:
	int16_t m_i;
};

class C2 {
public:
	C2(uint32_t p_i)
		: m_i(p_i) {}
	~C2() {
		std::cout << "C2 morrendo..." << std::endl;
	}

private:
	uint32_t m_i;
};

class C1 {
public:
	C1(uint32_t p_i, C3 * p_c3)
		: m_c2(p_i), m_c3(p_c3) {}
	~C1() {
		std::cout << "C1 morrendo..." << std::endl;
	}

private:
	C2 m_c2;
	C3 * m_c3;
};

int main() {
	C3 * x = new C3(-21);
	{
		C1 y(73456, x);
	}
	delete x;
}


