#include <stdint.h>
#include <string.h>
#include <stdio.h>

// *****************rc.h****************
typedef int16_t rc;
static const rc OK = 0;
static const rc PONTEIRO_NULO = 1;
static const rc STRING_MUITO_LONGO = 2;

// enum rc { OK = 0, PONTEIRO_NULO = 1, STRING_MUITO_LONGO = 2};

// *****************editora.h***********
 enum editora { DC = 1, MARVEL = 2 };

// *****************genero.h************
enum genero { ACAO = 100, SUSPENSE = 200};

// *****************personagem.h********
#define TAM_NOME 20
typedef struct  {
	char m_nome[TAM_NOME + 1];
	enum editora m_editora;
} personagem;

/*
.
.
.
personagem p1;
rc _rc = define_personagem(&p1, "homem-aranha o amigao", MARVEL);

if (_rc != OK) {
	printf("FALHA\n");
}
.
.
.
*/
rc define_personagem(personagem * p_personagem, 
	const char * p_nome, 
	enum editora p_editora);

/* 
Escrever uma fun��o que compare dois 'personagem'
Esta fun��o deve retornar 0 se forem iguais 
e diferente de 0 se forem diferentes

dica: strcmp
*/
//int16_t compara_personagem(personagem p1, personagem p2);

// *****************personagem.c********
rc define_personagem(personagem * p_personagem,
	const char * p_nome,
	enum editora p_editora) {
	if (p_personagem == NULL) {
		return PONTEIRO_NULO;
	}
	if (strlen(p_nome) > TAM_NOME) {
		return STRING_MUITO_LONGO;
	}

	p_personagem->m_editora = p_editora;
	strcpy(p_personagem->m_nome, p_nome);

	return OK;
}


// *****************estoria.h***********
#define TAM_TITULO 50
typedef struct {
	char m_titulo[TAM_TITULO + 1];
	enum genero m_genero;
	personagem * m_personagem;
}estoria;

rc define_estoria(estoria * p_estoria, const char * p_titulo,
	enum genero p_genero, personagem * p_personagem);

// *****************estoria.c***********
rc define_estoria(estoria * p_estoria, const char * p_titulo,
	enum genero p_genero, personagem * p_personagem) {
	if (p_estoria == NULL) {
		return PONTEIRO_NULO;
	}
	if (p_personagem == NULL) {
		return PONTEIRO_NULO;
	}
	if (strlen(p_titulo) > TAM_TITULO) {
		return STRING_MUITO_LONGO;
	}
	p_estoria->m_genero = p_genero;
	strcpy(p_estoria->m_titulo, p_titulo);
	p_estoria->m_personagem = p_personagem;
	return MARVEL;
}


// *****************personagens.h*******
#define NUM_PERSONAGENS 5
typedef struct {
	personagem m_colecao[NUM_PERSONAGENS];
}personagens;

// *****************estorias.h**********
#define NUM_ESTORIAS 5
typedef struct  {
	estoria m_colecao[NUM_ESTORIAS];
}estorias;


int main() {
	rc _rc = OK;

	personagem _home_aranha;
	_rc = define_personagem(&_home_aranha, "spider-man", MARVEL);
	if (_rc != OK) {
		printf("ERRO %d\n", _rc);
		return 1;
	}

	printf("Personagem: %s\n", _home_aranha.m_nome);
	
	_rc = define_personagem(&_home_aranha, "spider-man", SUSPENSE);


	return 0;

}