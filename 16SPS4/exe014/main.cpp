#include <iostream>
#include <string>
#include <memory>
#include <vector>


#include "cao.h"
#include "labrador.h"
#include "pastor_alemao.h"
#include "shitzu.h"



int main() {
	std::vector<std::shared_ptr<cao>> canil;

	canil.push_back(std::make_shared<shitzu>("xuxu"));
	canil.push_back(std::make_shared<labrador>("hugo"));
	canil.push_back(std::make_shared<pastor_alemao>("ragnar"));

	std::cout << "no canil tem..." << std::endl;
	canil[0]->late();
	canil[1]->late();
	canil[2]->late();


	return 0;

}