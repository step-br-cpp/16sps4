#include <stdio.h>
#include <stdint.h>
/* retorna a metade do valor 'p' */
double metade1(int32_t p);
/* coloca em '*q' a metade do valor de 'p' */
void metade2(int32_t p, double *q);
int main() {
	int32_t i1 = 37;
	double d1 = metade1(i1);
	int32_t i2 = 29;
	double d2 = 0.0;
	metade2(i2, &d2);
	printf("i1 = %d, d1 = %f, i2 = %d, d2 = %f\n", i1, d1, i2, d2);
}
double metade1(int32_t p) {
	return p / 2.0;
}

void metade2(int32_t p, double *q) {
	*q = p / 2.0;
}