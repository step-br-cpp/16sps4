
#include <cstdint>
#include <string>
#include <iostream>

namespace t16sps4 {



	// *****************rc.h****************
	enum class rc : int16_t {
		OK = 0,
		PONTEIRO_NULO = 1,
		STRING_MUITO_LONGO = 2
	};

	// enum rc { OK = 0, PONTEIRO_NULO = 1, STRING_MUITO_LONGO = 2};

	// *****************editora.h***********
	enum class editora : int8_t { DC = 1, MARVEL = 2 };

	std::string editora2str(editora e) {
		switch (e) {
		case editora::DC:
			return "DC";
		case editora::MARVEL:
			return "MARVEL";
		}
		return "INVALIDO";
	}


	// *****************personagem.h********

	class personagem {
	public:
		// declara��o
		personagem() = delete;
		personagem(const std::string & p_nome, editora p_editora);
		personagem(const std::string * p_nome, editora p_editora);

		bool operator == (const personagem & p_personagem) const;
		bool operator != (const personagem & p_personagem) const;

		bool operator == (editora p_editora) const;

		personagem & operator=(const personagem & p_personagem);

		const std::string & get_nome() const;
		const editora & get_editora() const;

		void set_nome(const std::string & p_nome);

	private:

		std::string m_nome;
		editora m_editora;
	};

	// defini��o
	personagem::personagem(const std::string & p_nome, editora p_editora)
		: m_nome(p_nome),
		m_editora(p_editora)
	{

	}

	personagem::personagem(const std::string * p_nome, editora p_editora) {
		m_nome = *p_nome;
		m_editora = p_editora;
	}

	bool personagem::operator == (const personagem & p_personagem) const {
		return (m_nome == p_personagem.m_nome);
	}

	bool personagem::operator != (const personagem & p_personagem) const {
		return (m_nome != p_personagem.m_nome);
	}

	bool personagem::operator == (editora p_editora) const {
		return m_editora == p_editora;
	}

	const std::string & personagem::get_nome() const {
		return m_nome;
	}

	const editora & personagem::get_editora() const {
		return m_editora;
	}

	void personagem::set_nome(const std::string & p_nome) {
		m_nome = p_nome;
	}

	personagem & personagem::operator=(const personagem & p_personagem) {
		if (this != &p_personagem) {
			m_nome = p_personagem.m_nome;
			m_editora = p_personagem.m_editora;
		}
		return *this;
	}

	// *****************genero.h************
	enum class genero : int16_t { ACAO = 100, SUSPENSE = 200 };


	struct estoria {

	public:
		estoria() = delete;

		estoria(const std::string & p_titulo, genero p_genero, personagem * p_personagem);

		bool operator == (const estoria & p_estoria) const;
		bool operator != (const estoria & p_estoria) const;

		const std::string & get_titulo() const;

		void set_genero(genero p_genero);

	private:
		std::string  m_titulo;
		genero m_genero;
		personagem * m_personagem;
	};

	estoria::estoria(const std::string & p_titulo, genero p_genero, personagem * p_personagem)
		: m_titulo(p_titulo)
		, m_genero(p_genero)
		, m_personagem(p_personagem) {}

	bool estoria::operator == (const estoria & p_estoria) const {
		return ((m_titulo == p_estoria.m_titulo) &&
			(*m_personagem == *(p_estoria.m_personagem)) 
			);
	}

	bool estoria::operator != (const estoria & p_estoria) const {
		return ((m_titulo != p_estoria.m_titulo) ||
			(*m_personagem != *(p_estoria.m_personagem))
			);
	}

	const std::string & estoria::get_titulo() const {
		return m_titulo;
	}

	void estoria::set_genero(genero p_genero) {
		m_genero = p_genero;
	}

} // namespace 


int main() {

	using namespace t16sps4;

	std::cout << "comecou C++!!! " << 44444 << std::endl;

	rc _rc = rc::OK;

	int i0(9);
	int i1 = 9;

	std::string s1("tempestade");

	editora em = editora::MARVEL;

	personagem p1(s1, em);
	personagem p2(&s1, editora::MARVEL);
	personagem p3("tempestade", editora::MARVEL);
	personagem p4(s1, em);
	personagem p5("flash", editora::DC);

	if (p1 == p3) { // p1.operator==(p3)
		std::cout << p1.get_nome() << " eh igual a " << p3.get_nome() << std::endl;
	}
	else {
		std::cout << p1.get_nome() << " NAO eh igual a " << p3.get_nome() << std::endl;
	}

	if (p1 == p5) { // p1.operator==(p5)
		std::cout << p1.get_nome() << " eh igual a " << p5.get_nome() << std::endl;
	}
	else {
		std::cout << p1.get_nome() << " NAO eh igual a " << p5.get_nome() << std::endl;
	}

	if (p1 != p5) { // p1.operator!=(p5)
		std::cout << p1.get_nome() << " NAO eh igual mesmo a " << p5.get_nome() << std::endl;
	}
	else {
		std::cout << p1.get_nome() << " eh igual a " << p5.get_nome() << std::endl;
	}

	if (p1 == editora::DC) { // p1.operator==(em)
		std::cout << editora2str(p1.get_editora()) << " eh igual a "
			<< editora2str(em) << std::endl;
	}
	else {
		std::cout << editora2str(p1.get_editora()) << " NAO eh igual a "
			<< static_cast<int16_t>(em) << std::endl;
	}

	estoria e1("guerra infinita", genero::ACAO, &p1);
	estoria e2("guerra infinita", genero::ACAO, &p2);

	if (e1 == e2) {
		std::cout << "e1 eh igual a e2" << std::endl;
	}
	else {
		std::cout << "e1 NAO eh igual a e2" << std::endl;
	}



}