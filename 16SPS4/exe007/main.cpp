#include <stdio.h>
#include <stdint.h>

#define TAM 6

/* Escrever uma f� que procure em
uma matriz de int16_t um valor.
Se encontrar, retorna o endere�o do valor,
se n�o, retorna NULL
*/
// DECLARA��O
int16_t * acha2(int16_t * inicio,
	int16_t * fim,
	int16_t val);



int main() {
	// declara��o
	int16_t m1[TAM];
	// declara��o + defini��o
	int16_t m2[TAM] = { 92, -3, 18, 401, -209, 6 };
	// acesso com []
	printf("m2[2] = %d\n", m2[2]);
	// acesso com endere�o
	printf("m2[2] = %d\n", *(m2 + 2));
	//// percorre com []
	//for (int16_t i1 = 0; i1 < TAM; ++i1) {
	//	printf("m2[%d] = %d\n", i1, m2[i1]);
	//}

	//int16_t * end = m2 + TAM;
	//for (int16_t * p1 = m2; p1 != end; ++p1) {
	//	printf("p1 = %d\n", *p1);
	//}

	// EXECU��O
	int16_t * ret1 = acha2(m2, m2 + TAM, 401);
	if (ret1 != NULL) {
		printf("val %d estah no endereco %x\n",
			401, ret1);
	}
	else {
		printf("nao achei %d\n", 401);
	}

	// EXECU��O
	int16_t * ret2 = acha2(m2, m2 + TAM, 39);
	if (ret2 != NULL) {
		printf("val %d estah no endereco %x\n",
			39, ret1);
	}
	else {
		printf("nao achei %d\n", 39);
	}

}

// DEFINI��O
int16_t * acha2(int16_t * inicio,
	int16_t * fim,
	int16_t val) {

	for (int16_t * p1 = inicio; p1 != fim; ++p1) {
		if (*p1 == val) {
			return p1;
		}
	}
	return NULL;
}
