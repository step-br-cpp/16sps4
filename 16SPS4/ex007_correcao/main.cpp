#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>



#define TAM 6

/*
Modifique a fun��o
'int16_t * acha2(int16_t * inicio, int16_t * fim, int16_t val)'
para achar o endere�o da primeira posi��o entre 'inicio' e 'fim'
que seja menor que 'val'.
*/
int16_t *acha2(int16_t *inicio, int16_t *fim, int16_t val);

/*
Com base na f� 'acha2' acima, crie a f� 'diferentes', que 
deve imprimir todos os valores diferentes de 'val'.
A f� 'diferentes' tem a seguinte assinatura:
'void diferentes(int16_t * inicio, int16_t * fim, int16_t val)'
*/


int main() {

	int16_t valor = 0;
	int16_t m[TAM] = { 92, -3, 18, 401, -209, 6 };


	printf("Digite o valor:\n");
	// erro ===> scanf("%d, &valor");
	scanf("%d", &valor);

	int16_t *ret = acha2(m, m + TAM, valor);
	if (ret != NULL) {
		printf("O valor %d est� no endere�o %x.\n", valor, ret);
	}
	else {
		printf("N�o achei o valor %d.", valor);
	}

	return 0;


}


int16_t *acha2(int16_t *inicio, int16_t *fim, int16_t val) {

	for (int16_t *i1 = inicio; i1 != fim; i1++) {
		if (*i1 < val) {
			return i1;
		}
	}

	return NULL;

}
