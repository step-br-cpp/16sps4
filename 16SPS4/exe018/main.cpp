
#include <chrono>
#include <iostream>
#include <fstream>
#include <string>

#define FILE_NAME "teste_grava_le.txt"

/** @brief Grava uma qtde de segundos em um arquivo

	@param p_file_name [in] nome do arquivo
	@param p_sec [in] a qtde de segundos

	@return true se conseguiu gravar, false caso contr�rio
*/
bool grava(const std::string & p_file_name, std::chrono::seconds p_sec);

/** @brief L� uma qtde de segundos em um arquivo

	@param p_file_name [in] nome do arquivo
	@param p_sec [out] a qtde de segundos

	@return true se conseguiu ler, false caso contr�rio
*/
bool le(const std::string & p_file_name, std::chrono::seconds & p_sec);



int main() {
	if (grava(FILE_NAME, std::chrono::seconds(90))) {
		std::cout << "SUCESSO!!!";
	}
	else {
		std::cout << "FALHA DE GRAVACAO!";
	}
	std::cout << std::endl;

	std::chrono::seconds secs(125);

	if (le(FILE_NAME, secs)) {
		std::cout << "SUCESSO!!! secs = " << secs.count();
	}
	else {
		std::cout << "FALHA DE LEITURA!";
	}
	std::cout << std::endl;
}


bool grava(const std::string & p_file_name, std::chrono::seconds p_sec) {
	try {
		std::ofstream file(p_file_name);

		file << p_sec.count();

		return true;
	}
	catch (std::exception & ex) {
		std::cerr << "ERRO!!! '" << ex.what() << "'" ; 
	}

	return false;
}

bool le(const std::string & p_file_name, std::chrono::seconds & p_sec) {
	try
	{
		std::ifstream file(p_file_name);
		uint64_t val;
		file >> val;
		p_sec = std::chrono::seconds(val);
		return true;
	}
	catch (const std::exception& ex)
	{
		std::cerr << "ERRO!!! '" << ex.what() << "'";
	}
	return false;
}










