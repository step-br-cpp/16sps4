#include <stdio.h>

#define MAX 1000

#define SAUDACAO "ola, mundo!"

#define SAIR 'S'

#define PI 3.14f

#define BOLSANARO 17
#define HADDAD 13
#define DACIOLO 51

#define max(n1,n2) (n1 > n2 ? n1 : n2)


int main(int argc, char **argv) {

	if (argc != 3) {
		printf("Sintaxe %s <nome-do-usuario> <cpf>\n", argv[0]);
		return 1;
	}

	printf("3.14 * 8 = %f\n", PI * 8);

	printf("max(4,18) = %d\n", max(4, 18));
	
	return 0;
}