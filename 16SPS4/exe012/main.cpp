/* 1 - 
Criar a estrutura 'celular' que contenha os seguintes campos:
- fabricante
- modelo
- qtde memoria
Sendo que os fabricantes podem ser 'motorola', 'samsung' e
'asus'

2 - Criar uma fun��o que inicialize uma vari�vel 'celular'
3 - Criar uma fun��o que imprima uma vari�vel 'celular'
4 - Inserir 5 vari�veis 'celular' em um vetor de celulares
5 - Escrever uma fun��o que percorra o vetor de celulares,
e que um dos par�metros � uma fun��o do tipo:
'void (*)(const celular *)'
6 - Execute a fun��o criada no t�pico 5, passando como 
par�metro a fun��o criada em 3
7 - Este programa receber� como par�metro o nome do dono
dos celulares, e deve ser impresso antes do t�pico 6


*/