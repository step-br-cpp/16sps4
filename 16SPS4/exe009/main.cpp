#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>

#define TAM 6

/*
Modifique a fun��o 'percorre1' para que ela possa pequisar
em um vetor por diferentes crit�rios.

Dica: a fun��o passada por par�metro para a nova 'percorre1'
deve poder usar diferentes valores como crit�rio.

Por exemplo: 
1 - percorrer o vetor procurando por todos os valores menores
que um valor informado
2 - percorrer o vetor procurando pelo primeiro valor maior
que um valor informado
3 - percorrer o vetor procurando por todos os valores m�ltiplos
de um valor informado
*/
void percorre1(int16_t *inicio, 
	int16_t *fim,
	uint8_t (*minha_funcao)(int16_t)
);

void percorre1(int16_t *inicio, int16_t *fim, uint8_t(*minha_funcao)(int16_t)) {
	for (int16_t *i1 = inicio; i1 != fim; i1++) {
		if (minha_funcao(*i1) == 1) {
			break;
		}
	}
}

uint8_t mf1(int16_t p_i) {
	if (p_i < 234) {
		printf("%d eh menor que 234\n", p_i);
	}
	return 0;
}

uint8_t mf4(int16_t p_i, int16_t p_valor) {
	if (p_i < p_valor) {
		printf("%d eh menor que 234\n", p_i);
	}
	return 0;
}

uint8_t mf2(int16_t p_i) {
	if ( (p_i % 2) == 0) {
		printf("%d eh par\n", p_i);
		return 1;
	}
	return 0;
}

uint8_t mf3(int16_t p_i) {
	if ((p_i >= 0) && (p_i < 234)) {
		printf("%d eh menor que 234 e maior que 0\n", p_i);
	}
	return 0;
}


int main() {

	int16_t m[TAM] = { 92, -3, 18, 401, -209, 6 };

	percorre1(m, m + TAM, mf1);

	percorre1(m, m + TAM, mf2);

	percorre1(m, m + TAM, mf3);

	return 0;


}



