#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <stdio.h>
#include <stdint.h>
/*  
calcula os valores anteriores e posteriores a um valor informado

p - � o valor informado
pre - conter� o valor anterior a p
pos - conter� o valor posterior a p
*/
void pre_pos(int32_t p, int32_t *pre, int32_t *pos);

int main() {
	// equivale ao 'p'
	int32_t p0 = 67; 
	// equivale ao 'pre'
	int32_t p1 = 0; 
	// equivale ao 'pos'
	int32_t p2 = 0;

	// aqui 'pre_pos' deve ser chamada

	printf("p0 = %d, p1 = %d, p2 = %d", p0, p1, p2);
}

void pre_pos(int32_t p, int32_t *pre, int32_t *pos) {

}