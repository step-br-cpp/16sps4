#include <stdio.h>
#include <stdint.h>

// DECLARA��O 
uint32_t duplo(uint16_t i);

void duplo1(uint16_t p, uint32_t * q);

int main() {
	float pi = 3.14f;
	// int 4 bytes com sinal
	int i = -26;
	// int 4 bytes sem sinal
	unsigned int i1 = 2566;
	// int 8 bytes sem sinal
	unsigned long long int i2 = 4324525235;
	// int 2 bytes com sinal
	short int r = -213;
	// int 1 byte sem sinal
	unsigned char i3 = 189;
	char c= 'w' ;
	double d = -0.023;

	// EXECU��O
	uint32_t i4 = duplo(9);

	uint16_t i0 = 19;
	uint32_t i5 = duplo(i0);

	uint16_t i6 = 220;
	uint32_t i7 = 11;
	duplo1(i6, &i7);

	printf("Turma 16SPS4 da Step, com Pi = %f, i = %d\n", pi, i);
	return 0;
}

// DEFINI��O
uint32_t duplo(uint16_t i) {
	return 2 * i;
}

void duplo1(uint16_t p, uint32_t * q) {
	(*q) = 2 * p;
}
