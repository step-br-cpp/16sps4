#include <stdio.h>
#include <stdint.h>

int main(int argc, char ** argv) {
	if (argc != 3) {
		printf("Sintaxe: %s <par1> <par2>\n", argv[0]);
		return 1;
	}

	for (int16_t i = 1; i < argc; ++i) {
		printf("Parametro %d = %s\n", i, argv[i]);
	}

}