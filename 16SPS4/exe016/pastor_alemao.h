#ifndef PASTOR_ALEMAO_H
#define PASTOR_ALEMAO_H

#include <string>
#include <iostream>

#include "cao.h"
class pastor_alemao : public cao
{
public:
	pastor_alemao()=delete;
	pastor_alemao(const std::string & nome);
	void late();
	~pastor_alemao();
};

#endif