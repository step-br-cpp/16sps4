#ifndef LABRADOR_H
#define LABRADOR_H

#include <string>
#include <iostream>

#include "cao.h"

class labrador : public cao
{
public:
	labrador() = delete;
	labrador(const std::string & p_nome);
	void late();
	~labrador();
};

#endif
