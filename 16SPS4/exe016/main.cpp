#include <iostream>
#include <string>
#include <memory>
#include <vector>


#include "cao.h"
#include "labrador.h"
#include "pastor_alemao.h"
#include "shitzu.h"

typedef std::vector<std::shared_ptr<cao>> canil_t;

/*
Procura por um 'cao', cujo nome � 'p_name', de 'p_begin' a 'p_end'
Se n�o achar, retorna 'p_end'
*/
canil_t::iterator acha_cao(canil_t::iterator p_ini
	, canil_t::iterator p_fim
	, const cao & p_cao) {
	for (canil_t::iterator _ite = p_ini; _ite != p_fim; ++_ite) {
		if ((*(*_ite)) == p_cao) {
			return _ite;
		}
	}
	return p_fim;
}

/*
Procura por todos os objetos 'cao', cuja raca � 'p_raca', de 'p_begin' a 'p_end'
Se n�o achar, o vetor deve estar vazio
*/
//std::vector<canil_t::iterator> acha_cao(canil_t::iterator p_ini
//	, canil_t::iterator p_fim
//	, raca p_raca) {
//	for (canil_t::iterator _ite = p_ini; _ite != p_fim; ++_ite) {
//		std::shared_ptr<cao> _cao_ptr = *_ite;
//		if (_cao_ptr->get_raca() == p_raca) {
//			//return _ite;
//		}
//	}
//}



int main() {
	
	canil_t canil;

	canil.push_back(std::make_shared<shitzu>("xuxu"));
	canil.push_back(std::make_shared<labrador>("hugo"));
	canil.push_back(std::make_shared<pastor_alemao>("ragnar"));
	//canil.push_back(std::make_shared<shitzu>("mariana"));
	//canil.push_back(std::make_shared<labrador>("josefa"));
	//canil.push_back(std::make_shared<pastor_alemao>("joao pedro"));

	std::cout << "no canil tem, usando �ndice..." << std::endl;

	for (canil_t::size_type i = 0; i < canil.size(); ++i) {
		std::shared_ptr<cao> _cao_ptr = canil[i];
		_cao_ptr->late();
	}

	std::cout << "no canil tem, usando iterator..." << std::endl;
	for (canil_t::iterator _ite = canil.begin(); _ite != canil.end(); ++_ite) {
		std::shared_ptr<cao> _cao_ptr = *_ite;
		_cao_ptr->late();
	}

	//canil_t::iterator ite_1 = acha_cao(canil.begin(), canil.end(), "spike");
	//canil_t::iterator ite_2 = acha_cao(canil.begin(), canil.end(), "josefa");

	//std::vector<canil_t::iterator> v_1 = acha_cao(canil.begin()
	//											, canil.end()
	//											, raca::LABRADOR);

	//std::vector<canil_t::iterator> v_2 = acha_cao(canil.begin()
	//	, canil.end()
	//	, raca::VIRA_LATA);



	return 0;

}