#include "cao.h"



cao::cao(const std::string & p_nome, raca p_raca)
	: m_nome(p_nome), m_raca(p_raca) 
{
}

cao::cao(const cao & p_cao)
	: m_nome(p_cao.m_nome), m_raca(p_cao.m_raca)
{
}

/*
cao c1("ze", LABRADOR);
cao c2 = c1; ==> c2.operator=(c1)

cao c3;
c3 = c1; ==> c3.operator=(c1)

c1 = c1; ==> c1.operator=(c1)
*/
cao & cao::operator=(const cao & p_cao)
{
	if (this != &p_cao) {
		m_nome = p_cao.m_nome;
		m_raca = p_cao.m_raca;
	}
	return *this;
}

bool cao::operator==(const cao & p_cao) const
{
	return ((m_nome == p_cao.m_nome) && (m_raca == p_cao.m_raca));
}

bool cao::operator!=(const cao & p_cao) const
{
	return ((m_nome != p_cao.m_nome) || (m_raca != p_cao.m_raca));
}

cao::~cao()
{
}

