#ifndef EXE0014_CAO_H
#define EXE0014_CAO_H

#include <cstdint>
#include <string>
#include <iostream>

enum class raca : uint16_t
{
	PASTOR_ALEMAO = 0,
	SHITZU = 1,
	LABRADOR = 2,
	VIRA_LATA = 3
};

class cao
{
public:
	cao() = delete;
	cao(const std::string & p_nome, raca p_raca);

	/*
	cao c1("ze", raca::LABRADOR);
	cao c2(c1);
	*/
	cao(const cao & p_cao);

	/*
	cao c1("ze", raca::LABRADOR);
	cao c2 = c1;

	c1 = c1; // evitar


	*/
	cao & operator=(const cao & p_cao);
	
	/*
	cao c1("ze", raca::LABRADOR);
	cao c2("miuxa", raca::SHITZU);

	if (c1 == c2) {
	...
	}
	else {
	....
	}
	*/
	bool operator==(const cao & p_cao) const;
	bool operator!=(const cao & p_cao) const;

	const std::string & get_nome() const {
		return m_nome;
	}

	raca get_raca() const {
		return m_raca;
	}

	virtual void late() = 0;

	virtual ~cao();

private:
	std::string m_nome;
	raca m_raca;
};

#endif