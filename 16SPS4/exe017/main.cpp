#include <algorithm>
#include <string>
#include <cstdint>
#include <iostream>
#include <vector>

enum class manufacturer : char {
	wv = 'V',
	bmw = 'B',
	ferrari = 'F',
	gm = 'G'
};

class car {
public:
	car() = delete;
	car(const std::string & p_model, manufacturer p_man)
		: m_model(p_model)
		, m_man(p_man) {}

	car(const car & p_car)
		: m_model(p_car.m_model)
		, m_man(p_car.m_man) {}

	car(car && p_car)
		: m_model(std::move(p_car.m_model))
		, m_man(std::move(p_car.m_man)) {}


	car & operator=(const car & p_car)
	{
		if (this != &p_car) {
			// para evitar
			// car c1("fusca", manufacture::vw);
			// c1 = c1;
			m_model = p_car.m_model;
			m_man = p_car.m_man;
		}
		return *this;
	}

	car & operator=(car && p_car)
	{
		if (this != &p_car) {
			// para evitar
			// car c1("fusca", manufacture::vw);
			// c1 = c1;
			m_model = std::move(p_car.m_model);
			m_man = std::move(p_car.m_man);
		}
		return *this;
	}

	manufacturer get_manufacturer() const {
		return m_man;
	}

	const std::string & get_model() const {
		return m_model;
	}

private:
	std::string m_model;
	manufacturer m_man;
};

typedef std::vector<car> cars_t;

// predicado como fun��o
bool is_ferrari(const car & p_car) {
	std::cout << "current car in container = " << p_car.get_model() << std::endl;
	return (p_car.get_manufacturer() == manufacturer::ferrari);
}

// predicado como function object
struct manufacture_criteria {
	manufacture_criteria(manufacturer p_man)
		: m_man(p_man) {}

	bool operator()(const car & p_car) {
		return p_car.get_manufacturer() == m_man;
	}

private:
	manufacturer m_man;
};

int main() {
	cars_t _cars;

	_cars.push_back(car("beagle", manufacturer::wv));
	_cars.push_back(car("kombi", manufacturer::wv));
	_cars.push_back(car("f50", manufacturer::ferrari));
	_cars.push_back(car("opala", manufacturer::gm));
	

	cars_t::iterator _ite = std::find_if(_cars.begin(), _cars.end(), is_ferrari);
	if (_ite == _cars.end()) {
		std::cout << "no car for 'is_ferrari'" << std::endl;
	}
	else {
		std::cout << "found car for 'is_ferrari': " << 
			_ite->get_model() << std::endl; 
	}

	manufacture_criteria _cri_1(manufacturer::ferrari);
	_ite = std::find_if(_cars.begin(), _cars.end(), _cri_1);
	if (_ite == _cars.end()) {
		std::cout << "no car for 'is_ferrari'" << std::endl;
	}
	else {
		std::cout << "found car for 'is_ferrari': " <<
			_ite->get_model() << std::endl;
	}



}