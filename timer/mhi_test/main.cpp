#include <chrono>
#include <memory>

#include <mhi/text_ui.h>
#include <bus/i_timer.h>

namespace bus {
	class timer_dummy : public bus::i_timer {
	public:
		timer_dummy() : i_timer() {}

		inline std::chrono::seconds load_last_timer() {
			return std::chrono::seconds(30);
		}
		inline void save_timer(std::chrono::seconds) {}
		inline void start(std::chrono::seconds) {}
		inline bool finished() { return true; }
	};
}

int main() {
	std::shared_ptr< bus::timer_dummy> _dummy
	      (std::make_shared<bus::timer_dummy>());

	mhi::text_ui ui(_dummy);

	ui.start();
}