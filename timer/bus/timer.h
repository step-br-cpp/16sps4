#ifndef BUS_TIMER_H
#define BUS_TIMER_H

#include <iostream>
#include <memory>

#include <bus/i_timer.h>
#include <per/i_per.h>

namespace bus {

	class timer : public i_timer
	{
	public:
		timer(std::shared_ptr<per::i_per> p_per);
		~timer();
		void start(std::chrono::seconds p_value);
		bool finished();
		std::chrono::seconds load_last_timer();
		void save_timer(std::chrono::seconds p_value);
	private:
		std::shared_ptr<per::i_per> m_per;
		uint32_t m_value = { 0 };
	};
}

#endif // !BUS_TIMER_H
