#ifndef BUS_I_TIMER_H
#define BUS_I_TIMER_H

#include <chrono>
#include <thread>

namespace bus {
	class i_timer {
	public:
		virtual std::chrono::seconds load_last_timer() = 0;
		virtual void save_timer(std::chrono::seconds p_value) = 0;
		virtual void start(std::chrono::seconds p_value) = 0;
		virtual bool finished() = 0;

	};
}

#endif
