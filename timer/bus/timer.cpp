#include <bus/timer.h>




namespace bus {
	timer::timer(std::shared_ptr<per::i_per> p_per)
		: m_per(p_per)
	{
	}

	timer::~timer()
	{
	}

	void timer::save_timer(std::chrono::seconds p_value) {
		m_per->save_timer(p_value);
	}

	std::chrono::seconds timer::load_last_timer() { 
		return m_per->load_last_timer();
	}

	void timer::start(std::chrono::seconds p_value) {
		m_value = static_cast<uint32_t>(p_value.count());
	}

	bool timer::finished() {
		if (m_value == 0) {
			return true;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		--m_value;
		return false;
	}
}