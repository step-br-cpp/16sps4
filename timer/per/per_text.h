#ifndef PER_PER_TEXT_H
#define PER_PER_TEXT_H

#include <string>

#include <per/i_per.h>

namespace per {

	class per_text : public i_per
	{
	public:
		per_text(const std::string & p_file_name);

		~per_text();

		std::chrono::seconds load_last_timer();
		void save_timer(std::chrono::seconds p_value);

	private:
		std::string m_file_name;
	};

}

#endif