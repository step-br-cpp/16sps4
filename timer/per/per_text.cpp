#include "per_text.h"

#include <fstream>
#include <iostream>

namespace per {

	per_text::per_text(const std::string & p_file_name)
		:m_file_name(p_file_name)
	{
	}

	per_text::~per_text()
	{
	}

	std::chrono::seconds per_text::load_last_timer() {
		try
		{
			std::ifstream file(m_file_name);
			if (!file.is_open()) {
				return std::chrono::seconds(30);
			}
			uint64_t val;
			file >> val;
			return std::chrono::seconds(val);
		}
		catch (const std::exception& ex)
		{
			std::cerr << "ERRO!!! '" << ex.what() << "'";
		}
		return std::chrono::seconds(0);
	}

	void per_text::save_timer(std::chrono::seconds p_value) {
		try {
			std::ofstream file(m_file_name);

			file << p_value.count();

		}
		catch (std::exception & ex) {
			std::cerr << "ERRO!!! '" << ex.what() << "'";
		}
	}

}