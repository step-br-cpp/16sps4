#ifndef PER_IPER_H
#define PER_IPER_H

#include <chrono>

namespace per {

	class i_per {
	public:
		virtual std::chrono::seconds load_last_timer() = 0;
		virtual void save_timer(std::chrono::seconds p_value) = 0;
	};

}

#endif
