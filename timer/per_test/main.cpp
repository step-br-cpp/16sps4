#include <per/per_text.h>

#include <chrono>
#include <iostream>

int main() {
	per::per_text _text("per_test.txt");

	std::chrono::seconds _t0(19);

	_text.save_timer(_t0);

	std::chrono::seconds _t1(_text.load_last_timer());

	if (_t0 == _t1) {
		std::cout << "PASSED";
	}
	else {
		std::cout << "FAILED";
	}

	std::cout << std::endl;

}