#ifndef MHI_TEXT_UI_H 
#define MHI_TEXT_UI_H

#include <chrono>
#include <memory>

#include <bus/i_timer.h>


namespace mhi {

	class text_ui {
	public:
		text_ui(std::shared_ptr<bus::i_timer> p_timer);
		~text_ui();

		text_ui(const text_ui &) = delete;
		text_ui(text_ui &&) = delete;

		text_ui &operator=(const text_ui &) = delete;
		text_ui &operator=(text_ui &&) = delete;

		void start();

	private:
		void execute_timer(std::chrono::seconds p_value);

		std::chrono::seconds load_last_timer();

		std::chrono::seconds new_timer();

		void save_timer(std::chrono::seconds p_value);

	private:
		std::shared_ptr<bus::i_timer> m_timer;
	};

}



#endif // !MHI_TEXT_UI_H 

