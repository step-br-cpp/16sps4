#include "text_ui.h"

#include <cstdint>
#include <iostream>
#include <thread>


namespace mhi {

	text_ui::text_ui(std::shared_ptr<bus::i_timer> p_timer)
		: m_timer(p_timer)
	{
	}

	text_ui::~text_ui()
	{
	}

	void text_ui::start()
	{
		std::chrono::seconds last_timer = load_last_timer();
		int16_t op = -1;
		while (op != 0) {
			std::cout << "\n\nUltimo timer: " << last_timer.count() << "\n\n\n";
			std::cout << "Escolha uma opcao: \n"
				<< "\t1 - Repetir timer \n"
				<< "\t2 - Novo timer\n"
				<< "\t0 - Sair" << std::endl;
			std::cin >> op;
			switch (op) {
			case 1:
				execute_timer(last_timer);
				break;

			case 2:
				last_timer = new_timer();
				save_timer(last_timer);
				execute_timer(last_timer);
				break;

			default:
				break;
			}
		}
	}

	void text_ui::execute_timer(std::chrono::seconds p_value)
	{
		//int32_t _value = static_cast<int32_t>(p_value.count());
		//while (_value > 0) {
		//	std::cout << ".";
		//	std::this_thread::sleep_for(std::chrono::seconds(1));
		//	--_value;
		//}

		m_timer->start(p_value);
		while (!m_timer->finished()) {
			std::cout << ".";
		}
		std::cout << std::endl;
	}

	std::chrono::seconds text_ui::load_last_timer()
	{
		return m_timer->load_last_timer();
	}

	std::chrono::seconds text_ui::new_timer()
	{
		uint32_t _new_time;
		std::cout << "Informe novo valor: ";
		std::cin >> _new_time;

		return std::chrono::seconds(_new_time);
	}

	void text_ui::save_timer(std::chrono::seconds p_value) {
		m_timer->save_timer(p_value);
	}
}