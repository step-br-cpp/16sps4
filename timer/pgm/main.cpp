#include <per/per_text.h>
#include <bus/timer.h>
#include <mhi/text_ui.h>

#include <memory>

static const char * nome_arq = "time.dat";

int main() {
	std::shared_ptr<per::per_text> _per
	(std::make_shared<per::per_text>(nome_arq));
	
	std::shared_ptr<bus::timer> _bus
	(std::make_shared<bus::timer>(_per));

	mhi::text_ui ui(_bus);

	ui.start();
}
